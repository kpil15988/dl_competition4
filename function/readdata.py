import csv

class import_data:
    def __init__(self,path):
        self.data_path = path["data_path"]
        self.data_path_valid = path["data_path_valid"]
        self.data_path_test = path["data_path_test"]
        self.data_csv_path = path["data_csv_path"]
        self.data_csv_path_valid = path["data_csv_path_valid"]
        self.data_csv_path_test = path["data_csv_path_test"]
        
        # train
        self.label = []
        self.image_name = []
        self.image_name_label = []

        # validation
        self.label_valid = []
        self.image_name_label_valid = []
        self.image_name_valid = []

        # test
        self.image_name_label_test = []
        self.image_name_test = []

    def read_data(self):
        #train
        
        with open(self.data_csv_path+'a.csv') as f:
            mycsv = csv.reader(f)
            for row in mycsv:
                label_orig = ('0'*(9-len(row[1]))+row[1])
                for i in range(9):
                    if int(label_orig[i]) == 0:
                        self.label.append(-1)
                    else:
                        self.label.append(int(label_orig[i]))


        with open(self.data_csv_path+'q.csv') as f:
            mycsv = csv.reader(f)
            for row in mycsv:
                for i in range(9):
                    self.image_name_label.append(self.data_path+row[0]+'.png')
                    self.image_name.append(self.data_path+row[i+1]+'.png')

        #valid
        self.label_valid = []
        with open(self.data_csv_path_valid+'a.csv') as f:
            mycsv = csv.reader(f)
            for row in mycsv:
                label_orig = ('0'*(9-len(row[1]))+row[1])
                self.label_valid.append([int(i) for i in label_orig])
        self.image_name_label_valid = []
        self.image_name_valid = []
        with open(self.data_csv_path_valid+'q.csv') as f:
            mycsv = csv.reader(f)
            for row in mycsv:
                self.image_name_label_valid.append(self.data_path_valid+row[0]+'.png')
                self.image_name_valid.append([self.data_path_valid+i+'.png' for i in row[1:]])

        #test
        self.image_name_label_test = []
        self.image_name_test = []
        with open(self.data_csv_path_test+'q.csv') as f:
            mycsv = csv.reader(f)
            for row in mycsv:
                self.image_name_label_test.append(self.data_path_test+row[0]+'.png')
                self.image_name_test.append([self.data_path_test+i+'.png' for i in row[1:]])
        return 
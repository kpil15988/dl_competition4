import tensorflow as tf
import numpy as np
import os
import time

class train:
    def __init__(self,hyper):
        self.BATCH_SIZE = hyper["BATCH_SIZE"]
        self.cnn_encoder = hyper["encoder1"]
        self.cnn_encoder2 = hyper["encoder2"]
        self.optimizer = tf.keras.optimizers.Adam(hyper["LR"])
        self.alpha = hyper["alpha"]
        self.steps_per_epoch = hyper["steps_per_epoch"]
        self.steps_per_epoch_valid = hyper["steps_per_epoch_valid"]
        
        # Set ckpt
        # one benefit of tf.train.Checkpoint() API is we can save everything seperately
        self.checkpoint_dir = './' + hyper["ckpt"]
        self.checkpoint_prefix = os.path.join(self.checkpoint_dir, hyper["ckpt"])
        self.ckp = tf.train.Checkpoint(optimizer=self.optimizer,cnn_encoder=self.cnn_encoder)
        self.ckpt_manager = tf.train.CheckpointManager(self.ckp, self.checkpoint_dir, max_to_keep=10)
        self.ckp.restore(hyper["latest_checkpoint"])
        self.cnn_encoder2.set_weights(self.cnn_encoder.get_weights())
        
    def cosine_similarity(self,v1, v2):
    # using cosine similarity as loss function
        cost = tf.reduce_sum(tf.multiply(v1, v2), 1) / (tf.sqrt(tf.reduce_sum(tf.multiply(v1, v1), 1)) * tf.sqrt(tf.reduce_sum(tf.multiply(v2, v2), 1)))
        return cost

    def loss(self,img_label,img,label):
    # Define a loss function
        loss_mean = tf.reduce_mean(tf.maximum(0., self.alpha -label* self.cosine_similarity(img_label, img) ))
        # loss_mean = tf.reduce_mean( -label* self.cosine_similarity(img_label, img))
        return loss_mean

    def acc(self,pred,label):
    # calculate validation accuracy
        boolean = tf.math.equal(pred,label)
        boolean_all = tf.reduce_all(boolean,axis = 1)
        accuracy = tf.math.reduce_mean(tf.cast(boolean_all,tf.float32))
        return accuracy

    @tf.function
    def train_step(self,img_label, img, label):
        # random noise for generator
        with tf.GradientTape() as tape:

            img_label_encoder = self.cnn_encoder(img_label)
            img_encoder = self.cnn_encoder2(img)
            cnn_loss = self.loss(img_label_encoder, img_encoder,label)

        grad = tape.gradient(cnn_loss, self.cnn_encoder.trainable_variables)
        self.optimizer.apply_gradients(zip(grad, self.cnn_encoder.trainable_variables))
        return cnn_loss
     
    def train(self,dataset,dataset_valid, epochs):

        for epoch in range(epochs):
            cnn_total_loss = 0
            cnn_total_loss_valid040 = 0
            cnn_total_loss_valid045 = 0
            cnn_total_loss_valid050 = 0
            cnn_total_loss_valid055 = 0
            cnn_total_loss_valid060 = 0
            cnn_total_loss_valid065 = 0
            cnn_total_loss_valid070 = 0
            cnn_total_loss_valid075 = 0
            cnn_total_loss_valid080 = 0
            
            # train step
            start = time.time()
            print("Start training")
            for image_label,image, label in dataset:
                cnn_loss = self.train_step(image_label,image, label)
                cnn_total_loss += cnn_loss
                
            self.cnn_encoder2.set_weights(self.cnn_encoder.get_weights())
            
            # validation step
            k = 1
            for image_label,image, label in dataset_valid:
                pred040 = []
                pred045 = []
                pred050 = []
                pred055 = []
                pred060 = []
                pred065 = []
                pred070 = []
                pred075 = []
                pred080 = []
                img_label_encoder = self.cnn_encoder(image_label)
                for i in range(9):                
                    img_encoder = self.cnn_encoder(image[:,i,:,:,:])
                    cnn_loss = (self.cosine_similarity(img_label_encoder, img_encoder))
                    
                    pred040.extend([int(i>0.40) for i in cnn_loss])
                    pred045.extend([int(i>0.45) for i in cnn_loss])
                    pred050.extend([int(i>0.50) for i in cnn_loss])
                    pred055.extend([int(i>0.55) for i in cnn_loss])
                    pred060.extend([int(i>0.60) for i in cnn_loss])
                    pred065.extend([int(i>0.65) for i in cnn_loss])
                    pred070.extend([int(i>0.70) for i in cnn_loss])
                    pred075.extend([int(i>0.75) for i in cnn_loss])
                    pred080.extend([int(i>0.80) for i in cnn_loss])
                    
                pred040 = np.reshape(pred040,(9,self.BATCH_SIZE)).T
                pred045 = np.reshape(pred045,(9,self.BATCH_SIZE)).T
                pred050 = np.reshape(pred050,(9,self.BATCH_SIZE)).T
                pred055 = np.reshape(pred055,(9,self.BATCH_SIZE)).T
                pred060 = np.reshape(pred060,(9,self.BATCH_SIZE)).T
                pred065 = np.reshape(pred065,(9,self.BATCH_SIZE)).T
                pred070 = np.reshape(pred070,(9,self.BATCH_SIZE)).T
                pred075 = np.reshape(pred075,(9,self.BATCH_SIZE)).T
                pred080 = np.reshape(pred080,(9,self.BATCH_SIZE)).T

                cnn_total_loss_valid040 += self.acc(pred040,label)
                cnn_total_loss_valid045 += self.acc(pred045,label)
                cnn_total_loss_valid050 += self.acc(pred050,label)
                cnn_total_loss_valid055 += self.acc(pred055,label)
                cnn_total_loss_valid060 += self.acc(pred060,label)
                cnn_total_loss_valid065 += self.acc(pred065,label)
                cnn_total_loss_valid070 += self.acc(pred070,label)
                cnn_total_loss_valid075 += self.acc(pred075,label)
                cnn_total_loss_valid080 += self.acc(pred080,label)
                
            valid_acc_list= np.array([cnn_total_loss_valid040.numpy(), cnn_total_loss_valid045.numpy(),
                             cnn_total_loss_valid050.numpy(), cnn_total_loss_valid055.numpy(),
                             cnn_total_loss_valid060.numpy(), cnn_total_loss_valid065.numpy(),
                             cnn_total_loss_valid070.numpy(), cnn_total_loss_valid075.numpy(), cnn_total_loss_valid080.numpy()])
            valid_acc_list = valid_acc_list/self.steps_per_epoch_valid
            
            cnn_total_loss_valid = max(valid_acc_list)
            print(list(zip(["040","045","050","055","060","065","070","075","080"],valid_acc_list)))
            
            
            time_tuple = time.localtime()
            time_string = time.strftime("%m/%d/%Y, %H:%M:%S", time_tuple)            
            print("Epoch {}, cnn_loss: {:.4f}, valid_acc: {:.6f},".format(epoch+1,
                                                                           cnn_total_loss/self.steps_per_epoch,
                                                                           cnn_total_loss_valid))
            print('Time for epoch {} is {:.4f} sec'.format(epoch+1, time.time()-start))

            # save the model
            self.ckpt_manager.save()
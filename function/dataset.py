import tensorflow as tf

# set parameter
BATCH_SIZE = 128
SHUFFLE_SIZE = 5000
IMAGE_HEIGHT = 128
IMAGE_WIDTH = 128
IMAGE_CHANNEL = 3

# Preprocessing data (let img become grayscale)
def image_generator(image_path):
    # load in the image according to image path
    img = tf.io.read_file(image_path)
    img = tf.image.decode_image(img, channels=3)
    img = tf.image.convert_image_dtype(img, tf.float32)
    img.set_shape([None, None, 3])
    img = tf.image.resize(img, size=[IMAGE_HEIGHT, IMAGE_WIDTH])
    img.set_shape([IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNEL])
    img = img*255
    img = tf.image.rgb_to_grayscale(img)
    #img = tf.image.grayscale_to_rgb(img) # vgg16不需要這個
    return img

# map function
def training_data_generator(label,image_path_label,image_path):
    # load in the image according to image path
    img = image_generator(image_path)
    img = tf.image.random_flip_left_right(img)
    img = tf.image.random_flip_up_down(img)
    #img = tf.image.rot90(img)
    #img = tf.image.random_contrast(img, lower = 0.2, upper = 1.2)
    
    img_label = image_generator(image_path_label)
    img_label = tf.image.random_flip_left_right(img_label)
    img_label = tf.image.random_flip_up_down(img_label)
    # img_label = tf.image.rot90(img_label)
    # img_label = tf.image.random_contrast(img_label, lower = 0.2, upper = 1.2)
    
    label = tf.cast(label, tf.float32)
    return img_label,img, label

def valid_data_generator(label,image_path_label,image_path):
    # load in the image according to image path
    img = []
    for i in range(9):
        img.append(image_generator(image_path[i]))

    img_label = image_generator(image_path_label) 
    label = tf.cast(label,tf.int32)
    return img_label,img, label

def testing_data_generator(image_path_label,image_path):
    # load in the image according to image path
    img = []
    for i in range(9):
        img.append(image_generator(image_path[i]))
    img_label = image_generator(image_path_label) 
    return img_label,img

# dataset function
def dataset_generator(label, image_name_label, image_name, batch_size, data_generator):
    dataset = tf.data.Dataset.from_tensor_slices((label, image_name_label, image_name))
    dataset = dataset.map(data_generator, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.shuffle(SHUFFLE_SIZE).batch(batch_size, drop_remainder=True)
    dataset = dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return dataset

def dataset_generator_valid(label, image_name_label, image_name, batch_size, data_generator):
    dataset = tf.data.Dataset.from_tensor_slices((label, image_name_label, image_name))
    dataset = dataset.map(data_generator, num_parallel_calls=tf.data.experimental.AUTOTUNE)
    dataset = dataset.batch(batch_size, drop_remainder=True)
    dataset = dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return dataset

def test_dataset_generator( image_name_label, image_name, batch_size, data_generator):
    dataset = tf.data.Dataset.from_tensor_slices((image_name_label, image_name))
    dataset = dataset.map(data_generator, num_parallel_calls=tf.data.experimental.AUTOTUNE).batch(batch_size)
    dataset = dataset.prefetch(buffer_size=tf.data.experimental.AUTOTUNE)
    return dataset

# Competition4 - Image CAPTCHA
In this competition, we aims to crack the image CAPTCHA by deep learning models.


## Introduction

![Schematic diagram](./Question.JPG)

There are a target and 9 questions in the above figure, respectively. We have to train a model and make it can recognize that which pictures in the questions are similar to a target. For example, in the above figure, a great model has to identify that picture 3,4,5 are the answers, those are similar to the target.


## Our main idea

The goal of this competition is like the function "Google Images". There are 2 main points in our training strategy.

#### Encode the image

Encode all pictures into vectors in the same size by CNN encoder. The reason is that we not only filter some unnecessary information, such as colors and size but also compressed image capacity.

Adopt grayscale vgg16 (untrainable) to extract features of pictures and add several trainable layers.

#### Cosine similarity

Use cosine similarity as the loss function and calculate cosine similarity between target and each question.

```math
Loss = max(0,-label * cosinesimilarity(img_{target},img_{question}))
```

label means the answer. 1 represents that two pictures are the same. -1 represents that two pictures are different.

If two pictures are similar, cosine similarity will be positive, then the loss function will be equal to 0.
If not, the loss function will be equal to cosine similarity.
We aims to make loss be smaller, thus the cosine similarity of two different picture should be close to 0 when updating.


#### Model structure

![Model structure](./Model_structure.JPG)















